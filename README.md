## kozet_fixed_point

*Note: kfp is getting a renewal. The old version can be found in the `legacy` branch.*

A header-only library for fixed-point numbers.

* `kozet_fixed_point/kfp.h` provides the types and the basic functionality.
* `kozet_fixed_point/kfp_extra.h` provides trigonometric functions that work
  on angles represented as 32-bit fractions of a turn.
* `kozet_fixed_point/random/kfp_wrapper.h` provides a wrapper around a random
  number generator.
* `kozet_fixed_point/kfp_string.h` provides functions to interact with text
  representations of fixed-point numbers.

### Rationale

It's sometimes necessary to have an alternative to floating-point numbers
that behaves the same across many platforms, regardless of the compiler used
to build a program or the hardware it runs on.

### Documentation

Requirements: C++17. The library also needs a 128-bit integer type. It tries
to use `__int128_t` and `__uint128_t` by default, but if they are not
available, then you must define the appropriate types yourself, such as the
ones from
[this library](https://www.boost.org/doc/libs/1_70_0/libs/multiprecision/doc/html/boost_multiprecision/tut/ints/cpp_int.html)

Type `make doc`, or visit [here](https://nagakawa.gitlab.io/kozet_fixed_point/).

#### Licence

   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.