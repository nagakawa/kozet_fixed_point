#pragma once
#ifndef KOZET_FIXED_POINT_KFP_STRING_H
#  define KOZET_FIXED_POINT_KFP_STRING_H

#  include <charconv>
#  include <iostream>
#  include <optional>
#  include <string_view>

#  include "./kfp.h"
#  include "./kfp_config.h"

namespace kfp {
  template<typename I, size_t d>
  inline std::ostream& operator<<(std::ostream& fh, const Fixed<I, d>& x) {
    return fh << x.toDouble();
  }

  template<typename I, size_t d>
  inline std::from_chars_result
  readFractionalPart(const char* first, const char* last, Fixed<I, d>& value) {
    using F = Fixed<I, d>;
    using U = std::make_unsigned_t<I>;
    U fDigits = 0;
    U fDivide = 1;
    const char* u = first;
    while (u < last) {
      // too many digits?
      if (fDivide >= std::numeric_limits<U>::max() / 10) break;
      if (*u < '0' || *u > '9') break; // non-digit?
      fDigits = 10 * fDigits + ((*u) - '0');
      fDivide *= 10;
      ++u;
    }
    if (fDivide == 1) {
      value = 0;
      return std::from_chars_result{first, {}};
    }
    U fPart = 0;
    for (size_t i = 0; i < d; ++i) {
      // Get `d` fractional bits
      // It's not safe to multiply by 2 yet --
      // if U is uint64_t,
      // then fDivide could be as high as 10000000000000000000,
      // with fDigits          as high as  9999999999999999999;
      // multiplying fDigits by 2 would overflow in this case.
      // Instead, compare to fDivide / 2.
      bool is1 = fDigits >= fDivide / 2;
      fPart = (fPart << 1) | is1;
      if (is1) fDigits -= fDivide / 2;
      // Now fDigits < fDivide / 2, so we can safely multiply by 2.
      fDigits <<= 1;
    }
    value = F::raw((I) fPart);
    return std::from_chars_result{u, {}};
  }

  /**
   * \brief Parse a string into a fixed-point number.
   *
   * The interface works much like `std::from_chars`.
   *
   * \param first A pointer to the first character of the string.
   * \param last A pointer right after the last character of the string.
   * \param value The parsed value will be stored here if successful.
   * \return A `std::from_chars_result`.
   *
   * \todo This function always rounds the results toward zero. It should
   *   be able to use other rounding modes.
   */
  template<typename I, size_t d>
  inline std::from_chars_result
  fromChars(const char* first, const char* last, Fixed<I, d>& value) {
    using F = Fixed<I, d>;
    constexpr size_t n = CHAR_BIT * sizeof(I);
    while (first < last && (*first == ' ' || *first == '\n' || *first == '\t' ||
                            *first == '\v')) {
      ++first;
    }
    bool isNegative = first < last && *first == '-';
    if (isNegative) ++first;
    I iPart = 0;
    if (first >= last) {
      return std::from_chars_result{last, std::errc::invalid_argument};
    }
    if (*first == '.' && first + 1 == last) { // Reject lone '.'
      return std::from_chars_result{last, std::errc::invalid_argument};
    }
    const char* u = first + 1;
    if (*first != '.') {
      std::from_chars_result resultInt = std::from_chars(first, last, iPart);
      if (resultInt.ec != std::errc()) { return resultInt; }
      if ((n == d && iPart != 0) || (iPart >> (n - d)) != 0) {
        return std::from_chars_result{resultInt.ptr,
                                      std::errc::result_out_of_range};
      }
      if (resultInt.ptr >= last || *(resultInt.ptr) != '.') {
        value = iPart;
        return std::from_chars_result{resultInt.ptr, {}};
      }
      u = resultInt.ptr + 1;
    }
    F fPart;
    std::from_chars_result resultFrac = readFractionalPart(u, last, fPart);
    F composed = iPart + fPart;
    value = isNegative ? -composed : composed;
    return std::from_chars_result{resultFrac.ptr, {}};
  }

  /// Convenience method for converting from strings.
  template<typename F> std::optional<F> fromString(std::string_view sv) {
    F x;
    auto res = fromChars(sv.begin(), sv.end(), x);
    if (res.ec != std::errc()) return std::nullopt;
    return x;
  }

  /**
   * \brief Convert a string into a fixed-point number.
   *
   * The interface works much like `std::to_chars`.
   *
   * \tparam round The rounding mode to use.
   * \param first A pointer to the first character of the buffer.
   * \param last A pointer right after the last character of the buffer.
   * \param value The value to stringify.
   * \return A `std::to_chars_result`.
   */
  template<
      typename I, size_t d,
      std::float_round_style round = std::float_round_style::round_to_nearest>
  inline std::to_chars_result
  toChars(char* first, char* last, Fixed<I, d> value) {
    static_assert(round != std::float_round_style::round_indeterminate);
    using F = Fixed<I, d>;
    using D = DoubleType<I>;
    bool neg = false;
    if (value < 0) {
      if (first == last) { // no more room
        return std::to_chars_result{last, std::errc::value_too_large};
      }
      *first = '-';
      ++first;
      value = -value;
      neg = true;
    }
    std::to_chars_result resultInt = std::to_chars(first, last, value.floor());
    if (resultInt.ec != std::errc()) { return resultInt; }
    first = resultInt.ptr;
    F frac = value - value.floor();
    // no need to print any decimal places
    if (frac == 0) return std::to_chars_result{first, {}};
    // we have a fractional part, so we need to print decimal places
    if (first == last)
      return std::to_chars_result{last, std::errc::value_too_large};
    *first = '.';
    ++first;
    /*
      Our algorithm is based on section 3 of
      http://kurtstephens.com/files/p372-steele.pdf, where we have

      - n = d
      - b = 2
      - B = 10

      Thankfully, this is the most complex machinery we'll need.
    */
    using M = Fixed<D, d + 1>;
    size_t k = 0;
    M r = frac;
    M m = M::raw(1);
    D u;
    while (true) {
      ++k;
      M ur = r * 10;
      r = ur - ur.floor();
      u = ur.floor();
      m *= 10;
      if (!(r >= m && r <= 1 - m)) break;
      if (first == last)
        return std::to_chars_result{last, std::errc::value_too_large};
      *first = '0' + u;
      ++first;
    }
    // TODO: support different rounding modes
    if (first == last)
      return std::to_chars_result{last, std::errc::value_too_large};
    using RS = std::float_round_style;
    // clang-format off
    int roundMode =
        round == RS::round_toward_zero ? 0 :
        round == RS::round_to_nearest ? 1 :
        round == RS::round_toward_infinity ? (neg ? 0 : 2) :
        round == RS::round_toward_neg_infinity ? (neg ? 2 : 0) : 0;
    // clang-format on
    if (roundMode == 0) {
      *first = '0' + u;
    } else if (roundMode == 1) {
      if (r < M(1) / 2) {
        *first = '0' + u;
      } else {
        *first = '1' + u;
      }
    } else if (roundMode == 2) {
      if (r < M(1) / 10) {
        *first = '0' + u;
      } else {
        *first = '1' + u;
      }
    }
    ++first;
    return std::to_chars_result{first, {}};
  }

  /// Convenience method for converting to strings.
  template<typename F> inline std::string toString(F x) {
    char buf[256];
    auto res = toChars(std::begin(buf), std::end(buf), x);
    if (res.ec != std::errc()) abort();
    return std::string(buf, res.ptr - buf);
  }
}

#endif
