#pragma once
#ifndef KOZET_FIXED_POINT_KFP_CONFIG_H
#  define KOZET_FIXED_POINT_KFP_CONFIG_H

/**
 * \brief Configuration options for kozet_floating_point.
 *
 * - `KFP_I_LITERALLY_HAVE_NO_FPU`: If true, then try to avoid floating-point
 *   operations whenever possible. Otherwise, use them when they are faster
 *   but don't affect results.
 * - `KFP_PEDANTIC_DISPLAY`: If true, then don't use floating-point operations
 *   for conversions to strings. Conversions from strings will not use float
 *   operations, even when this is set to false.
 */

#  ifndef KFP_I_LITERALLY_HAVE_NO_FPU
#    define KFP_I_LITERALLY_HAVE_NO_FPU false
#  endif

#  ifndef KFP_PEDANTIC_DISPLAY
#    define KFP_PEDANTIC_DISPLAY false
#  endif

#endif
