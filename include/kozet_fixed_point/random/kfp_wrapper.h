/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#pragma once
#ifndef KOZET_FIXED_POINT_RANDOM_KFP_WRAPPER_H
#  define KOZET_FIXED_POINT_RANDOM_KFP_WRAPPER_H

#  include <stddef.h>

#  include <random>
#  include <type_traits>

#  include "../kfp.h"

namespace kfp {
  /**
   * \brief A wrapper around a random number generator.
   *
   * \tparam Generator A type satisfying
   * [`RandomNumberEngine`](https://en.cppreference.com/w/cpp/named_req/RandomNumberEngine)
   *
   * This class "wraps" a random number engine to provide methods for
   * producing the most common types of (pseudo-)random numbers, as well
   * as fixed-point numbers. Unlike standard library random number
   * distributions, such generation is reproducible.
   */
  template<typename Generator> class Wrapper {
  public:
    using Underlying = Generator;
    Wrapper() : held(0), nBitsLeft(0) {}
    Wrapper(Underlying&& u) : underlying(std::move(u)), held(0), nBitsLeft(0) {}
    /// Get the underlying random number engine.
    const Underlying& getUnderlying() const { return underlying; }
    /// Get the underlying random number engine.
    Underlying& getUnderlying() { return underlying; }
    /// Seed the random number generator.
    void seed(typename Underlying::result_type seed) { underlying.seed(seed); }
    /// Seed the random number generator.
    template<class SSeq> void seed(SSeq& sseq) { underlying.seed(sseq); }
    /**
     * \brief Get some bits.
     *
     * \tparam T The integral type to store the bits in.
     * \tparam nBits The number of bits to generate.
     */
    template<typename T, unsigned nBits> T getBits() {
      static_assert(
          nBits <= CHAR_BIT * sizeof(T),
          "T is not large enough to hold that many bits");
      std::make_unsigned_t<T> bits = 0;
      unsigned nBitsNeeded = nBits;
      while (nBitsNeeded > 0) {
        if (nBitsLeft == 0) regenerate();
        unsigned nBitsExtracted =
            (nBitsNeeded < nBitsLeft) ? nBitsNeeded : nBitsLeft;
        // Flag to check if shifting by `nBitsExtracted` is UB
        bool isResultFull = nBitsExtracted == CHAR_BIT * sizeof(T);
        bool isHeldExhausted = nBitsExtracted >= CHAR_BIT * sizeof(held);
        bits = isResultFull ? 0 : (bits << nBitsExtracted);
        T newBits =
            isHeldExhausted ? held : (T)(held & ((1ULL << nBitsExtracted) - 1));
        held = isHeldExhausted ? 0 : (held >> nBitsExtracted);
        bits |= newBits;
        nBitsLeft -= nBitsExtracted;
        nBitsNeeded -= nBitsExtracted;
      }
      return bits;
    }
    template<typename T>
    using RequireUnsignedInt =
        typename std::enable_if_t<std::is_unsigned<T>::value, int>;
    template<typename T>
    using RequireSignedInt =
        typename std::enable_if_t<std::is_signed<T>::value, void>;
    template<typename T>
    using RequireFixedPoint = typename std::enable_if_t<isFixedPoint<T>, void>;
    // Based on this: https://crypto.stackexchange.com/a/5721
    /**
     * \brief Get a random integer in the range [0, max).
     */
    template<
        typename T,
        RequireUnsignedInt<T>* dummy = nullptr>
    T next(T max) { // Here, n = 1 << tBits; k = max.
      using D = DoubleType<T>;
      constexpr int tBits = CHAR_BIT * sizeof(T);
      D r = 0, s = 1;
      while (true) {
        D x = getBits<T, tBits>();
        r = (r << tBits) | x;
        s <<= tBits;
        D thold = (s / max) * max;
        if (r > thold) {
          r -= thold;
          s -= thold;
        } else {
          return (T)(r % max);
        }
      }
    }
    /**
     * \brief Get a random integer in the range [min, max).
     */
    template<typename T, RequireUnsignedInt<T>* dummy = nullptr>
    T next(T min, T max) {
      return min + next(max - min);
    }
    /**
     * \brief Get a random integer in the range [min, max).
     */
    template<typename T, RequireSignedInt<T>* dummy = nullptr>
    T next(T min, T max) {
      using U = typename std::make_unsigned_t<T>;
      auto range = (U)(max - min);
      auto unsignedResult = next(range) + (U) min;
      return (T) unsignedResult;
    }
    /**
     * \brief Get a random fixed-point number in the range [min, max).
     */
    template<typename F, RequireFixedPoint<F>* dummy = nullptr>
    F next(F min, F max) {
      return F::raw(
          next<typename F::Underlying>(min.underlying, max.underlying));
    }
    /// Get a random unsigned integer from the set of its possible values.
    template<typename T, RequireUnsignedInt<T>* dummy = nullptr> T next() {
      return getBits<T, CHAR_BIT * sizeof(T)>();
    }
    /// Get a random signed integer from the set of its possible values.
    template<typename T, RequireSignedInt<T>* dummy = nullptr> T next() {
      return (T) next<std::make_unsigned_t<T>>();
    }
    /// Get a random fixed-point number from the set of its possible values.
    template<typename F, RequireFixedPoint<F>* dummy = nullptr> F next() {
      return F::raw(next<typename F::Underlying>());
    }

  private:
    Underlying underlying;
    typename Underlying::result_type held;
    unsigned nBitsLeft;
    void regenerate() {
      held = underlying();
      nBitsLeft = CHAR_BIT * sizeof(typename Underlying::result_type);
    }
  };
}

#endif // KOZET_FIXED_POINT_RANDOM_KFP_WRAPPER_H
