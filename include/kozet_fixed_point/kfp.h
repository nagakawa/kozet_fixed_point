/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#pragma once
#ifndef KOZET_FIXED_POINT_KFP_H
#  define KOZET_FIXED_POINT_KFP_H

#  include <limits.h>
#  include <math.h>
#  include <stddef.h>
#  include <stdint.h>

#  include <limits>
#  include <type_traits>
#  include <utility>

#  include "./kfp_config.h"

/** \mainpage kozet_fixed_point
 *
 * \section intro_sec Introduction & Rationale
 *
 * kozet_floating_point, or kfp for short, is a header-only library for
 * fixed-point numbers.
 *
 * It's sometimes necessary to have an alternative to floating-point numbers
 * that behaves the same across many platforms, regardless of the compiler used
 * to build a program or the hardware it runs on. TDR is one of the projects
 * that needs such a number type.
 *
 * \section install_sec Using the library
 *
 * This library requires C++17. It also uses 128-bit integer types; if it
 * doesn't think it has `__int128_t` and `__uint128_t` types available, then
 * you'll need to modify `kfp.h` to define the appropriate types yourself.
 * [This
 * library](https://www.boost.org/doc/libs/1_70_0/libs/multiprecision/doc/html/boost_multiprecision/tut/ints/cpp_int.html)
 * might be useful.
 *
 * kfp is a header-only library, so copy the `include/` directory wherever you'd
 * like and you're set!
 *
 *  * `kozet_fixed_point/kfp.h` provides the types and the basic functionality.
 *  * `kozet_fixed_point/kfp_extra.h` provides trigonometric functions that work
 *    on angles represented as 32-bit fractions of a turn.
 *  * `kozet_fixed_point/random/kfp_wrapper.h` provides a wrapper around a
 * random number generator.
 *  * `kozet_fixed_point/kfp_string.h` provides functions to interact with text
 * representations of fixed-point numbers.
 *
 * To start, look at \ref kfp::Fixed.
 *
 * \section hacking Notes for hackers
 *
 * ~~don't or I'll call the cops~~
 *
 * The tests for the library are found in `test/test.cpp`, and the CMake target
 * is called `mytest` (with the executable at `build/mytest`).
 *
 * Do note that some of the tests use floating-point arithmetic for convenience.
 */
namespace kfp {
  // Check for prescence of __int128
#  ifdef __SIZEOF_INT128__
  using int128_t = __int128_t;
  using uint128_t = __uint128_t;
#  else
// Use a library that provides a 128-bit int type with the usual operations
// and define kfp::int128_t and kfp::uint128_t to the signed/unsigned variant
// respectively if not done already.
#    error \
        "No 128-bit integer type found! Replace this line with the appropriate definitions!"
#  endif
  /// \cond false
  template<typename T> struct DTI;
  template<> struct DTI<int8_t> { typedef int_fast16_t type; };
  template<> struct DTI<int16_t> { typedef int_fast32_t type; };
  template<> struct DTI<int32_t> { typedef int_fast64_t type; };
  template<> struct DTI<int64_t> { typedef int128_t type; };
  template<> struct DTI<uint8_t> { typedef uint_fast16_t type; };
  template<> struct DTI<uint16_t> { typedef uint_fast32_t type; };
  template<> struct DTI<uint32_t> { typedef uint_fast64_t type; };
  template<> struct DTI<uint64_t> { typedef uint128_t type; };
  template<typename T> struct DTIX;
  template<> struct DTIX<int8_t> { typedef int16_t type; };
  template<> struct DTIX<int16_t> { typedef int32_t type; };
  template<> struct DTIX<int32_t> { typedef int64_t type; };
  template<> struct DTIX<int64_t> { typedef int128_t type; };
  template<> struct DTIX<uint8_t> { typedef uint16_t type; };
  template<> struct DTIX<uint16_t> { typedef uint32_t type; };
  template<> struct DTIX<uint32_t> { typedef uint64_t type; };
  template<> struct DTIX<uint64_t> { typedef uint128_t type; };
  template<typename T> struct HTI;
  template<> struct HTI<int8_t> { typedef int8_t type; };
  template<> struct HTI<int16_t> { typedef int8_t type; };
  template<> struct HTI<int32_t> { typedef int16_t type; };
  template<> struct HTI<int64_t> { typedef int32_t type; };
  template<> struct HTI<int128_t> { typedef int64_t type; };
  template<> struct HTI<uint8_t> { typedef uint8_t type; };
  template<> struct HTI<uint16_t> { typedef uint8_t type; };
  template<> struct HTI<uint32_t> { typedef uint16_t type; };
  template<> struct HTI<uint64_t> { typedef uint32_t type; };
  template<> struct HTI<uint128_t> { typedef uint64_t type; };
  /// \endcond
  template<typename T> using DoubleType = typename DTI<T>::type;
  template<typename T> using DoubleTypeExact = typename DTIX<T>::type;
  template<typename T> using HalfType = typename HTI<T>::type;
  // TODO: when we drop support for pre-C++20, then replace this with a left
  // shift
  template<typename I> constexpr I leftShift(I a, size_t b) {
    using T = std::decay_t<I>;
    if constexpr (std::is_same_v<T, int128_t> || std::is_same_v<T, uint128_t>) {
      using U = uint128_t;
      return (I)(((U) a) << b);
    } else {
      using U = std::make_unsigned_t<T>;
      return (I)(((U) a) << b);
    }
  }
  /**
   * \brief A class for a binary fixed-point number.
   *
   * See [here](https://en.wikipedia.org/wiki/Fixed-point_arithmetic)
   * for background information about fixed-point arithmetic.
   *
   * This type of number type can be useful when reproducibility is necessary.
   * For instance, tdr uses them for gameplay-related code in order to
   * avoid differences in floating-point handling that could desync replays.
   *
   * \tparam I The underlying integer type.
   * \tparam d The number of bits after the decimal point.
   *
   * Aliases are defined for some types according to the following convention
   * (where `n` is `CHAR_BIT * sizeof(I)`):
   *
   * - Fractional types (`I` is unsigned, `d == n`) are named `frac`<i>n</i>.
   * - Signed types (`I` is signed, `d < n`) are named
   * `s`<i>(n - d)</i>`_`<i>d</i>.
   * - Unsigned types (`I` is unsigned, `d < n`) are named
   * `u`<i>(n - d)</i>`_`<i>d</i>.
   *
   * There are several ways to construct a fixed-point number:
   *
   * - Initialise from an integer value
   * - Initialise from a fixed-point type with both fewer integral digits and
   *   fractional bits
   * - Use the \ref raw method to initialise with an underlying value
   * - For the types with aliases, use a user-defined literal (see \ref
   *   kfp::literals)
   *
   * For the first two options, fixed-point numbers can be assigned from such
   * types as well.
   *
   * Note that there are no implicit conversions to or from floating-point
   * numbers.
   *
   * The operators `+ - * / << >>` are available with their obvious meanings,
   * as well as their assignment variants. The relations `== != < <= > >=` are
   * defined as well.
   *
   * `*` can be applied as long as the underlying integer type is the same
   * between the left-hand and right-hand sides, even if the number of
   * fractional bits is different. `>>` and `<<` take integer arguments for the
   * right-hand side. Other operators can be applied only to the exact same
   * types.
   *
   * Note that the result of `*` when the left-hand side is a fixed-point number
   * is that of the left-hand side.
   *
   * Also see \ref longMultiply, which returns a type big enough to hold the
   * product.
   */
  template<typename I, size_t d> struct Fixed {
    using F = kfp::Fixed<I, d>;
    using Underlying = I;
    /// The representation of a fixed-point number as an integer.
    I underlying;
    // Sanity checks
    // Is I an integer?
    static_assert(
        std::numeric_limits<I>::is_integer || std::is_same_v<I, int128_t> ||
            std::is_same_v<I, uint128_t>,
        "I must be an integer type");
    template<typename I2, size_t d2>
    using HasIntegerBits = std::enable_if_t<(d2 < CHAR_BIT * sizeof(I2))>;
    template<typename I2, size_t d2>
    using HasNoIntegerBits =
        std::enable_if_t<(d2 >= CHAR_BIT * sizeof(I2)), int>;
    // Constructors
    constexpr Fixed() = default; // : underlying(0) {}
    /**
     * \brief Construct a value from an integer.
     *
     * The underlying value is the argument shifted left by the number of bits
     * after the radix. That is, `kfp::s16_16(3).underlying == (3 << 16)`.
     */
    template<
        typename I2 = I, size_t d2 = d, HasIntegerBits<I2, d2>* dummy = nullptr>
    constexpr Fixed(I value) noexcept : underlying(leftShift(value, d)) {}
    /**
     * \brief Construct a value from an integer.
     *
     * This overload is used when the number has no integral bits. Thus, the
     * underlying value is always zero.
     */
    template<
        typename I2 = I, size_t d2 = d,
        HasNoIntegerBits<I2, d2>* dummy = nullptr>
    constexpr Fixed(I value) noexcept : underlying(0) {
      (void) value;
    }
    constexpr Fixed(const F& value) = default;
    // Implicit cast from smaller type
    template<typename I2, size_t d2, typename I3, size_t d3>
    using IsConvertible = std::enable_if_t<
        (std::numeric_limits<I2>::digits - d2) >=
                (std::numeric_limits<I3>::digits - d3) &&
            d2 >= d3,
        int>;
    template<typename I2, size_t d2, typename I3, size_t d3>
    using IsNonConvertible = std::enable_if_t <
                                 (std::numeric_limits<I2>::digits - d2) <
                                 (std::numeric_limits<I3>::digits - d3) ||
                             d2<d3>;
    /// Implicitly cast from a type with fewer integral digits and fractional
    /// bits.
    template<
        typename I2, size_t d2, typename I3 = I, size_t d3 = d,
        IsConvertible<I3, d3, I2, d2>* dummy = nullptr>
    constexpr Fixed(const Fixed<I2, d2>& other) noexcept {
      static_assert(
          (std::numeric_limits<I>::digits - d) >=
              (std::numeric_limits<I2>::digits - d2),
          "Cannot implicitly cast into a type with fewer integral digits");
      static_assert(
          d >= d2,
          "Cannot implicitly cast into a type with fewer fractional bits");
      // How much left should we shift?
      underlying = other.underlying;
      if (fractionalBits() >= other.fractionalBits())
        underlying =
            leftShift(underlying, fractionalBits() - other.fractionalBits());
      else
        underlying >>= (other.fractionalBits() - fractionalBits());
    }
    /// Explicitly cast from a type that either has more integral digits or
    /// more fractional bits.
    template<
        typename I2, size_t d2, typename I3 = I, size_t d3 = d,
        IsNonConvertible<I3, d3, I2, d2>* dummy = nullptr>
    explicit constexpr Fixed(const Fixed<I2, d2>& other) noexcept {
      // How much left should we shift?
      if (fractionalBits() >= other.fractionalBits())
        underlying = leftShift(
            other.underlying, fractionalBits() - other.fractionalBits());
      else
        underlying =
            other.underlying >> (other.fractionalBits() - fractionalBits());
    }
    /**
     * \brief Construct a fixed-point number from an underlying value.
     *
     * For example, `kfp::s16_16::raw(5835)` represents the number
     * 5835 / 65536.
     */
    constexpr static F raw(I underlying) noexcept {
      F ret(0);
      ret.underlying = underlying;
      return ret;
    }
    /**
     * \brief Construct a fixed-point number from a ratio of two integers.
     */
    constexpr static F fraction(I num, I denom) noexcept {
      using D = DoubleType<I>;
      D x = num;
      x = leftShift(x, d);
      x /= denom;
      return raw((I) x);
    }
    /// Get the number of integral bits, including the sign bit.
    static constexpr size_t integralBits() noexcept {
      return CHAR_BIT * sizeof(I) - d;
    }
    /// Get the number of integral digits, excluding the sign bit.
    static constexpr size_t integralDigits() noexcept {
      return std::numeric_limits<I>::digits - d;
    }
    /// Get the number of fractional bits.
    static constexpr size_t fractionalBits() noexcept { return d; }
    // Operator defines
    F& operator=(const F& other) = default;
    /// Assign from an integer.
    constexpr F& operator=(const I& i) noexcept {
      underlying = leftShift(i, d);
      return *this;
    }
    template<typename I2, size_t d2>
    constexpr F& operator=(const Fixed<I2, d2>& other) noexcept {
      static_assert(
          integralDigits() >= other.integralDigits(),
          "Cannot implicitly cast into a type with fewer integral digits");
      static_assert(
          fractionalBits() >= other.fractionalBits(),
          "Cannot implicitly cast into a type with fewer fractional bits");
      // How much left should we shift?
      underlying = other.underlying;
      if (fractionalBits() >= other.fractionalBits())
        underlying =
            leftShift(underlying, fractionalBits() - other.fractionalBits());
      else
        underlying >>= (other.fractionalBits() - fractionalBits());
      return *this;
    }
#  define DEF_OP_BOILERPLATE(o) \
    template<typename X> \
    constexpr F operator o(const X& other) const noexcept { \
      F ret = *this; \
      ret o## = other; \
      return ret; \
    }
#  define DEF_OP_BOILERPLATE2(o) \
    DEF_OP_BOILERPLATE(o) \
    template< \
        typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr F& operator o##=(const I2& other) noexcept { \
      *this o## = F(other); \
      return *this; \
    }
#  define DEF_OP(o) \
    DEF_OP_BOILERPLATE2(o) \
    constexpr F& operator o##=(const F& other) noexcept
    // end define
    DEF_OP(+) {
      underlying += other.underlying;
      return *this;
    }
    DEF_OP(-) {
      underlying -= other.underlying;
      return *this;
    }
    DEF_OP(&) {
      underlying &= other.underlying;
      return *this;
    }
    DEF_OP(|) {
      underlying |= other.underlying;
      return *this;
    }
    DEF_OP(^) {
      underlying ^= other.underlying;
      return *this;
    }
    DEF_OP_BOILERPLATE(*)
    template<
        typename I2, size_t d2, typename I3 = I, size_t d3 = d,
        IsConvertible<I3, d3, I2, d2>* dummy = nullptr>
    constexpr F& operator*=(const Fixed<I2, d2>& other) noexcept {
      DoubleType<I> prod = ((DoubleType<I>) underlying) * other.underlying;
      underlying = (I)(prod >> other.fractionalBits());
      return *this;
    }
    template<size_t d2>
    constexpr F& operator*=(const Fixed<I, d2>& other) noexcept {
      DoubleType<I> prod = ((DoubleType<I>) underlying) * other.underlying;
      underlying = (I)(prod >> other.fractionalBits());
      return *this;
    }
    constexpr F& operator*=(I other) noexcept {
      underlying *= other;
      return *this;
    }
    DEF_OP_BOILERPLATE(/)
    constexpr F& operator/=(const F& other) noexcept {
      DoubleType<I> dividend = leftShift((DoubleType<I>) underlying, d);
      underlying = (I)(dividend / other.underlying);
      return *this;
    }
    constexpr F& operator/=(I other) noexcept {
      underlying /= other;
      return *this;
    }
    DEF_OP_BOILERPLATE(<<)
    constexpr F& operator<<=(int shift) noexcept {
      underlying = leftShift(underlying, shift);
      return *this;
    }
    DEF_OP_BOILERPLATE(>>)
    constexpr F& operator>>=(int shift) noexcept {
      underlying >>= shift;
      return *this;
    }
    constexpr F operator-() const noexcept {
      F ret = *this;
      ret.underlying = -ret.underlying;
      return ret;
    }
    constexpr F operator~() const noexcept {
      F ret = *this;
      ret.underlying = ~ret.underlying;
      return ret;
    }
#  undef DEF_OP
#  undef DEF_OP_BOILERPLATE
#  undef DEF_OP_BOILERPLATE2
    /**
     * \brief Explicitly convert to an integer type.
     *
     * This just calls \ref floor().
     */
    explicit constexpr operator I() const noexcept { return floor(); }
    /**
     * \brief Get the floor of the number.
     *
     * The number is rounded down to the next integer.
     */
    constexpr I floor() const noexcept {
      if constexpr (d < sizeof(I) * CHAR_BIT) {
        return underlying >> d;
      } else {
        return underlying >= 0 ? 0 : -1;
      }
    }
    /**
     * \brief Get the floor of the number.
     *
     * The number is rounded up to the next integer.
     */
    constexpr I ceil() const noexcept {
      [[maybe_unused]] constexpr I one = 1;
      if constexpr (d == 0) {
        return floor(); // there can be no decimal part
      } else if constexpr (d >= sizeof(I) * CHAR_BIT) {
        return underlying > 0 ? 1 : 0;
      } else {
        return (underlying + (one << d) - 1) >> d;
      }
    }
    /**
     * \brief Round the number to the nearest integer.
     */
    constexpr I round() const noexcept {
      [[maybe_unused]] constexpr I one = 1;
      if constexpr (d == 0) {
        return floor(); // there can be no decimal part
      } else if constexpr (d == sizeof(I) * CHAR_BIT) {
        return (((DoubleType<I>) underlying) + (one << (d - 1))) >> d;
      } else {
        return (underlying + (one << (d - 1))) >> d;
      }
    }
    /// Convert to a double.
    constexpr double toDouble() const noexcept {
      return ((double) underlying) / exp2(d);
    }
  };
  template<typename I, size_t d> struct DTI<Fixed<I, d>> {
    typedef Fixed<DoubleType<I>, 2 * d> type;
  };
  template<typename I, size_t d> struct DTIX<Fixed<I, d>> {
    typedef Fixed<DoubleTypeExact<I>, 2 * d> type;
  };
  /**
   * \brief Multiply two fixed-point numbers and return the result in a type
   * that is large enough.
   *
   * The underlying types of the two numbers must be the same, but the number
   * of bits after the radix can be different.
   */
  template<typename I, size_t d, size_t d2>
  constexpr Fixed<DoubleTypeExact<I>, d + d2>
  longMultiply(Fixed<I, d> a, Fixed<I, d2> b) noexcept {
    DoubleTypeExact<I> p = a.underlying;
    p *= b.underlying;
    return Fixed<DoubleTypeExact<I>, d + d2>::raw(p);
  }
  /**
   * \brief Multiply two integers and return the result in a type
   * that is large enough.
   */
  template<
      typename I,
      std::enable_if_t<std::is_integral<I>::value, void*> dummy = nullptr>
  constexpr DoubleTypeExact<I> longMultiply(I a, I b) noexcept {
    DoubleTypeExact<I> p = a;
    p *= b;
    return static_cast<DoubleTypeExact<I>>(a) * b;
  }
  // Relational operators
#  define DEF_RELATION(o) \
    template<typename I, size_t d> \
    constexpr bool operator o( \
        const Fixed<I, d>& a, const Fixed<I, d>& b) noexcept { \
      return a.underlying o b.underlying; \
    }
  DEF_RELATION(==)
  DEF_RELATION(!=)
  DEF_RELATION(<)
  DEF_RELATION(<=)
  DEF_RELATION(>)
  DEF_RELATION(>=)
#  undef DEF_RELATION
#  define DEF_RELATION_I(o, o2) \
    template< \
        typename I, size_t d, typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr bool operator o(const Fixed<I, d>& a, I2 b) noexcept { \
      return a.floor() o2 b && a.underlying o leftShift((I) b, d); \
    } \
    template< \
        typename I, size_t d, typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr bool operator o(I2 a, const Fixed<I, d>& b) noexcept { \
      return a o2 b.floor() && leftShift((I) a, d) o b.underlying; \
    }
  DEF_RELATION_I(==, ==)
  DEF_RELATION_I(<, <=)
  DEF_RELATION_I(>, >=)
#  undef DEF_RELATION_I
#  define DEF_RELATION_INV(o, o2) \
    template< \
        typename I, size_t d, typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr bool operator o(const Fixed<I, d>& a, I2 b) noexcept { \
      return !(a o2 b); \
    } \
    template< \
        typename I, size_t d, typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr bool operator o(I2 a, const Fixed<I, d>& b) noexcept { \
      return !(a o2 b); \
    }
  DEF_RELATION_INV(!=, ==)
  DEF_RELATION_INV(<=, >)
  DEF_RELATION_INV(>=, <)
#  undef DEF_RELATION_INV
#  define BACK_OPERATOR(o) \
    template< \
        typename I, size_t d, typename I2, \
        std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr> \
    constexpr kfp::Fixed<I, d> operator o(I2 n, kfp::Fixed<I, d> x) noexcept { \
      return x o n; \
    }
  BACK_OPERATOR(+)
  BACK_OPERATOR(*)
  template<
      typename I, size_t d, typename I2,
      std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr>
  constexpr kfp::Fixed<I, d> operator-(I2 n, kfp::Fixed<I, d> x) noexcept {
    return -(x - n);
  }
  template<
      typename I, size_t d, typename I2,
      std::enable_if_t<std::is_integral<I2>::value, void*> dummy = nullptr>
  constexpr kfp::Fixed<I, d> operator/(I2 n, kfp::Fixed<I, d> x) noexcept {
    return kfp::Fixed<I, d>(n) / x;
  }
#  undef BACK_OPERATOR
  // end
  using s16_16 = Fixed<int32_t, 16>;
  using u16_16 = Fixed<uint32_t, 16>;
  using s2_30 = Fixed<int32_t, 30>;
  using s34_30 = Fixed<int64_t, 30>;
  using frac32 = Fixed<uint32_t, 32>;
  // User-defined literals
  /**
   * \brief Convert a decimal string into a fixed-point number.
   *
   * * Negative numbers are not yet supported.
   * * The decimal point is optional.
   * * It is legal to omit any digit before the decimal point or after it.
   *   If only the decimal point is present, then the result is zero.
   * * The empty string results in zero.
   * * If there are any invalid characters, then the result is undefined.
   */
  template<typename I, size_t d> constexpr Fixed<I, d> convert(const char* s) {
    using F = Fixed<I, d>;
    // Decimal point present?
    const char* t = s;
    while (*t != '\0' && *t != '.') ++t;
    bool hasDecimal = *t == '.';
    const char* u = s;
    I iPart = 0;
    while (u != t) {
      iPart *= 10;
      iPart += (*u) - '0';
      ++u;
    }
    F res(iPart);
    if (hasDecimal && t[1] != '\0') {
      using U = std::make_unsigned_t<I>;
      U fDigits = 0;
      U fDivide = 1;
      u = t + 1;
      while (*u != '\0') {
        if (fDivide >= std::numeric_limits<U>::max() / 10) break;
        fDigits = 10 * fDigits + ((*u) - '0');
        fDivide *= 10;
        ++u;
      }
      U fPart = 0;
      for (size_t i = 0; i < d; ++i) {
        // Get `d` fractional bits
        // It's not safe to multiply by 2 yet --
        // if U is uint64_t,
        // then fDivide could be as high as 10000000000000000000,
        // with fDigits          as high as  9999999999999999999;
        // multiplying fDigits by 2 would overflow in this case.
        // Instead, compare to fDivide / 2.
        bool is1 = fDigits >= fDivide / 2;
        fPart = (fPart << 1) | is1;
        if (is1) fDigits -= fDivide / 2;
        // Now fDigits < fDivide / 2, so we can safely multiply by 2.
        fDigits <<= 1;
      }
      // Now the `d` low bits of fPart are set.
      res += F::raw((I) fPart);
    }
    return res;
  }
#  define DEFINE_OPERATOR_LITERAL(type) \
    constexpr type operator""_##type(const char* s, size_t) { \
      return convert<type::Underlying, type::fractionalBits()>(s); \
    }
  /**
   * \brief Operator literals for every fixed-point type with an alias.
   *
   * You can use them with
   *
   * <code>
   * using namespace kfp::literals;
   * </code>
   */
  namespace literals {
    DEFINE_OPERATOR_LITERAL(s16_16)
    DEFINE_OPERATOR_LITERAL(u16_16)
    DEFINE_OPERATOR_LITERAL(s2_30)
    DEFINE_OPERATOR_LITERAL(s34_30)
    DEFINE_OPERATOR_LITERAL(frac32)
  }
#  undef DEFINE_OPERATOR_LITERAL
  template<typename T> struct IsFixedPoint {
    constexpr static bool value = false;
  };
  template<typename I, size_t d> struct IsFixedPoint<Fixed<I, d>> {
    constexpr static bool value = true;
  };
  template<typename T> constexpr bool isFixedPoint = IsFixedPoint<T>::value;
}

template<typename I, size_t d> struct std::numeric_limits<kfp::Fixed<I, d>> {
  static constexpr int ctl10o2(int x) noexcept { return (31 * x + 99) / 100; }
  using F = kfp::Fixed<I, d>;
  static constexpr bool is_specialized = true;
  static constexpr bool is_signed = std::numeric_limits<I>::is_signed;
  static constexpr bool is_integer = false;
  static constexpr bool is_exact = true;
  static constexpr bool has_infinity = false;
  static constexpr bool has_quiet_NaN = false;
  static constexpr bool has_signaling_NaN = false;
  static constexpr bool has_denorm = false;
  static constexpr bool has_denorm_loss = false;
  // TODO set a proper value for this
  static constexpr std::float_round_style round_style =
      std::float_round_style::round_indeterminate;
  static constexpr bool is_iec559 = false;
  static constexpr bool is_bounded = std::numeric_limits<I>::is_bounded;
  static constexpr bool is_modulo = std::numeric_limits<I>::is_modulo;
  static constexpr int digits = std::numeric_limits<I>::digits;
  static constexpr int digits10 = std::numeric_limits<I>::digits10;
  static constexpr int max_digits10 =
      ctl10o2(std::numeric_limits<I>::digits) + 1;
  static constexpr int radix = 2;
  static constexpr int min_exponent = -d + 1;
  static constexpr int max_exponent = std::numeric_limits<I>::digits - d;
  static constexpr int min_exponent10 = ctl10o2(-d + 1);
  static constexpr int max_exponent10 =
      ctl10o2(std::numeric_limits<I>::digits - d);
  static constexpr bool traps = true;
  static constexpr bool tinyness_before = false;
  static constexpr F min() noexcept {
    return F::raw(std::numeric_limits<I>::min());
  }
  static constexpr F max() noexcept {
    return F::raw(std::numeric_limits<I>::max());
  }
  static constexpr F lowest() noexcept {
    return F::raw(std::numeric_limits<I>::lowest());
  }
  static constexpr F epsilon() noexcept { return F::raw(1); }
  static constexpr F round_error() noexcept { return F::raw(1 << (d - 1)); }
  static constexpr F infinity() noexcept { return F(0); }
  static constexpr F quiet_NaN() noexcept { return F(0); }
  static constexpr F signaling_NaN() noexcept { return F(0); }
  static constexpr F denorm_min() noexcept { return F(0); }
};

template<typename I, size_t d> struct std::hash<kfp::Fixed<I, d>> {
  using F = kfp::Fixed<I, d>;
  size_t operator()(const F& f) const noexcept {
    return std::hash<I>()(f.underlying);
  }
};

#endif // KOZET_FIXED_POINT_KFP_H
