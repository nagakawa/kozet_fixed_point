/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#pragma once
#ifndef KOZET_FIXED_POINT_KFP_EXTRA_LUT_H
#  define KOZET_FIXED_POINT_KFP_EXTRA_LUT_H

#  include <assert.h>
#  include <stdio.h>
#  include <stdlib.h>

#  include "./kfp.h"
#  include "./kfp_config.h"
#  include "./kfp_extra.h"

namespace kfp {
  /*
    say (^256).map({round sin(tau*$_/256) * 2**32 % 2**32}).map({sprintf
    "0x%08xu", $_}).join(", ")
  */
  static constexpr int32_t sinLut[] = {
      0x00000000,  0x0192155f,  0x0323ecbe,  0x04b54825,  0x0645e9af,
      0x07d59396,  0x09640837,  0x0af10a22,  0x0c7c5c1e,  0x0e05c135,
      0x0f8cfcbe,  0x1111d263,  0x1294062f,  0x14135c94,  0x158f9a76,
      0x17088531,  0x187de2a7,  0x19ef7944,  0x1b5d100a,  0x1cc66e99,
      0x1e2b5d38,  0x1f8ba4dc,  0x20e70f32,  0x223d66a8,  0x238e7673,
      0x24da0a9a,  0x261feffa,  0x275ff452,  0x2899e64a,  0x29cd9578,
      0x2afad269,  0x2c216eaa,  0x2d413ccd,  0x2e5a1070,  0x2f6bbe45,
      0x30761c18,  0x317900d6,  0x32744493,  0x3367c090,  0x34534f41,
      0x3536cc52,  0x361214b0,  0x36e5068a,  0x37af8159,  0x387165e3,
      0x392a9642,  0x39daf5e8,  0x3a8269a3,  0x3b20d79e,  0x3bb6276e,
      0x3c42420a,  0x3cc511d9,  0x3d3e82ae,  0x3dae81cf,  0x3e14fdf7,
      0x3e71e759,  0x3ec52fa0,  0x3f0ec9f5,  0x3f4eaafe,  0x3f84c8e2,
      0x3fb11b48,  0x3fd39b5a,  0x3fec43c7,  0x3ffb10c1,  0x40000000,
      0x3ffb10c1,  0x3fec43c7,  0x3fd39b5a,  0x3fb11b48,  0x3f84c8e2,
      0x3f4eaafe,  0x3f0ec9f5,  0x3ec52fa0,  0x3e71e759,  0x3e14fdf7,
      0x3dae81cf,  0x3d3e82ae,  0x3cc511d9,  0x3c42420a,  0x3bb6276e,
      0x3b20d79e,  0x3a8269a3,  0x39daf5e8,  0x392a9642,  0x387165e3,
      0x37af8159,  0x36e5068a,  0x361214b0,  0x3536cc52,  0x34534f41,
      0x3367c090,  0x32744493,  0x317900d6,  0x30761c18,  0x2f6bbe45,
      0x2e5a1070,  0x2d413ccd,  0x2c216eaa,  0x2afad269,  0x29cd9578,
      0x2899e64a,  0x275ff452,  0x261feffa,  0x24da0a9a,  0x238e7673,
      0x223d66a8,  0x20e70f32,  0x1f8ba4dc,  0x1e2b5d38,  0x1cc66e99,
      0x1b5d100a,  0x19ef7944,  0x187de2a7,  0x17088531,  0x158f9a76,
      0x14135c94,  0x1294062f,  0x1111d263,  0x0f8cfcbe,  0x0e05c135,
      0x0c7c5c1e,  0x0af10a22,  0x09640837,  0x07d59396,  0x0645e9af,
      0x04b54825,  0x0323ecbe,  0x0192155f,  0x00000000,  -0x0192155f,
      -0x0323ecbe, -0x04b54825, -0x0645e9af, -0x07d59396, -0x09640837,
      -0x0af10a22, -0x0c7c5c1e, -0x0e05c135, -0x0f8cfcbe, -0x1111d263,
      -0x1294062f, -0x14135c94, -0x158f9a76, -0x17088531, -0x187de2a7,
      -0x19ef7944, -0x1b5d100a, -0x1cc66e99, -0x1e2b5d38, -0x1f8ba4dc,
      -0x20e70f32, -0x223d66a8, -0x238e7673, -0x24da0a9a, -0x261feffa,
      -0x275ff452, -0x2899e64a, -0x29cd9578, -0x2afad269, -0x2c216eaa,
      -0x2d413ccd, -0x2e5a1070, -0x2f6bbe45, -0x30761c18, -0x317900d6,
      -0x32744493, -0x3367c090, -0x34534f41, -0x3536cc52, -0x361214b0,
      -0x36e5068a, -0x37af8159, -0x387165e3, -0x392a9642, -0x39daf5e8,
      -0x3a8269a3, -0x3b20d79e, -0x3bb6276e, -0x3c42420a, -0x3cc511d9,
      -0x3d3e82ae, -0x3dae81cf, -0x3e14fdf7, -0x3e71e759, -0x3ec52fa0,
      -0x3f0ec9f5, -0x3f4eaafe, -0x3f84c8e2, -0x3fb11b48, -0x3fd39b5a,
      -0x3fec43c7, -0x3ffb10c1, -0x40000000, -0x3ffb10c1, -0x3fec43c7,
      -0x3fd39b5a, -0x3fb11b48, -0x3f84c8e2, -0x3f4eaafe, -0x3f0ec9f5,
      -0x3ec52fa0, -0x3e71e759, -0x3e14fdf7, -0x3dae81cf, -0x3d3e82ae,
      -0x3cc511d9, -0x3c42420a, -0x3bb6276e, -0x3b20d79e, -0x3a8269a3,
      -0x39daf5e8, -0x392a9642, -0x387165e3, -0x37af8159, -0x36e5068a,
      -0x361214b0, -0x3536cc52, -0x34534f41, -0x3367c090, -0x32744493,
      -0x317900d6, -0x30761c18, -0x2f6bbe45, -0x2e5a1070, -0x2d413ccd,
      -0x2c216eaa, -0x2afad269, -0x29cd9578, -0x2899e64a, -0x275ff452,
      -0x261feffa, -0x24da0a9a, -0x238e7673, -0x223d66a8, -0x20e70f32,
      -0x1f8ba4dc, -0x1e2b5d38, -0x1cc66e99, -0x1b5d100a, -0x19ef7944,
      -0x187de2a7, -0x17088531, -0x158f9a76, -0x14135c94, -0x1294062f,
      -0x1111d263, -0x0f8cfcbe, -0x0e05c135, -0x0c7c5c1e, -0x0af10a22,
      -0x09640837, -0x07d59396, -0x0645e9af, -0x04b54825, -0x0323ecbe,
      -0x0192155f};
  template<> struct Sincos<TrigAlgorithm::lut> {
    constexpr static std::pair<s2_30, s2_30> sincos(frac32 t) noexcept {
      uint32_t u = t.underlying;
      uint32_t prog = u & 0xFF'FFFF;
      uint32_t i = u >> 24;
      // cos(theta) = sin(pi/2 - theta)
      // or cosTurn(a) = sinTurn(0.25 - a)
      int32_t c0 = sinLut[(256 + 64 - i) % 256];
      int32_t c1 = sinLut[(256 + 64 - i - 1) % 256];
      int32_t s0 = sinLut[i];
      int32_t s1 = sinLut[(i + 1) % 256];
      auto interpolate = [](int32_t a, int32_t b, uint32_t prog) {
        return (int32_t)(
            ((((int64_t) a) * ((1 << 24) - prog)) + (((int64_t) b) * prog)) >>
            24);
      };
      return std::pair(
          s2_30::raw(interpolate(c0, c1, prog)),
          s2_30::raw(interpolate(s0, s1, prog)));
    }
  };
}

#endif
