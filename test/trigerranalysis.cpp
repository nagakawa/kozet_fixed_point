/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

#include "kozet_fixed_point/kfp.h"
#include "kozet_fixed_point/kfp_extra.h"
#include "kozet_fixed_point/kfp_extra_lut.h"
#include "kozet_fixed_point/kfp_string.h"
#include "kozet_fixed_point/random/kfp_wrapper.h"

constexpr unsigned resolution = 0x100'0000;
constexpr unsigned plotStep = 0x1000;

struct Record {
  // std::vector<kfp::s2_30> sines, cosines;
  std::vector<double> cosineErrors, sineErrors;
  const char* name;
  double timeTaken;
  double averageErrorCos, averageErrorSin;
  double maxErrorCos, maxErrorSin;
  Record(
      const char* name,
      std::pair<kfp::s2_30, kfp::s2_30> (*callback)(kfp::frac32),
      const std::vector<double>& fCosines, const std::vector<double>& fSines) :
      name(name) {
    std::vector<kfp::s2_30> cosines, sines;
    cosines.resize(resolution);
    sines.resize(resolution);
    clock_t cl1 = clock();
    for (unsigned i = 0; i < resolution; ++i) {
      auto [c, s] = callback(kfp::frac32::fraction(i, resolution));
      cosines[i] = c;
      sines[i] = s;
    }
    clock_t cl2 = clock();
    timeTaken = ((double) (cl2 - cl1)) / CLOCKS_PER_SEC * 1e9 / resolution;
    cosineErrors.resize(resolution);
    sineErrors.resize(resolution);
    for (unsigned i = 0; i < resolution; ++i) {
      cosineErrors[i] = fabs(cosines[i].toDouble() - fCosines[i]);
    }
    for (unsigned i = 0; i < resolution; ++i) {
      sineErrors[i] = fabs(sines[i].toDouble() - fSines[i]);
    }
    maxErrorCos = *std::max_element(cosineErrors.begin(), cosineErrors.end());
    maxErrorSin = *std::max_element(sineErrors.begin(), sineErrors.end());
    averageErrorCos =
        std::accumulate(cosineErrors.begin(), cosineErrors.end(), 0.0) /
        resolution;
    averageErrorSin =
        std::accumulate(sineErrors.begin(), sineErrors.end(), 0.0) / resolution;
  }
};

int main() {
  // Pregenerate array with reference values
  std::vector<double> fCosines, fSines;
  fCosines.resize(resolution);
  fSines.resize(resolution);
  for (unsigned i = 0; i < resolution; ++i) {
    fCosines[i] = cos(2 * M_PI * ((double) i) / resolution);
  }
  for (unsigned i = 0; i < resolution; ++i) {
    fSines[i] = sin(2 * M_PI * ((double) i) / resolution);
  }
  std::vector<Record> records;
  records.emplace_back(
      "CORDIC method",
      [](kfp::frac32 t) { return kfp::sincos<kfp::TrigAlgorithm::cordic>(t); },
      fCosines, fSines);
  records.emplace_back(
      "Bhaskara I's approximation",
      [](kfp::frac32 t) {
        return kfp::sincos<kfp::TrigAlgorithm::bhaskara>(t);
      },
      fCosines, fSines);
  records.emplace_back(
      "Lookup table approximation",
      [](kfp::frac32 t) { return kfp::sincos<kfp::TrigAlgorithm::lut>(t); },
      fCosines, fSines);
  /* local */ {
    std::ofstream dfh("trigerrs_cos.dat");
    for (unsigned i = 0; i < resolution; i += plotStep) {
      dfh << ((double) i) / resolution << " ";
      for (const Record& r : records) {
        unsigned count = std::min(i + plotStep, resolution) - i;
        double avg = std::accumulate(
                         r.cosineErrors.begin() + i,
                         r.cosineErrors.begin() + i + count, 0.0) /
                     count;
        dfh << avg << " ";
      }
      dfh << "\n";
    }
  }
  /* local */ {
    std::ofstream dfh("trigerrs_sin.dat");
    for (unsigned i = 0; i < resolution; i += plotStep) {
      dfh << ((double) i) / resolution << " ";
      for (const Record& r : records) {
        unsigned count = std::min(i + plotStep, resolution) - i;
        double avg = std::accumulate(
                         r.sineErrors.begin() + i,
                         r.sineErrors.begin() + i + count, 0.0) /
                     count;
        dfh << avg << " ";
      }
      dfh << "\n";
    }
  }
  std::ofstream fh("output.gnu");
  fh << "set multiplot layout 2,1\n";
  fh << "set title \"kfp trigonometric function error analysis (cosine)\"\n";
  fh << "set key outside vertical spacing 2.5\n";
  fh << "set logscale y\n";
  fh << "plot 'trigerrs_cos.dat'";
  for (size_t i = 0; i < records.size(); ++i) {
    const Record& r = records[i];
    if (i != 0) { fh << ", ''"; }
    fh << " using 1:" << (i + 2);
    fh << " title \"" << r.name << "\\n(" << r.timeTaken << "ns per value)\"";
    fh << " with lines";
  }
  fh << "\n";
  fh << "set title \"kfp trigonometric function error analysis (sine)\"\n";
  fh << "plot 'trigerrs_sin.dat'";
  for (size_t i = 0; i < records.size(); ++i) {
    const Record& r = records[i];
    if (i != 0) { fh << ", ''"; }
    fh << " using 1:" << (i + 2);
    fh << " title \"" << r.name << "\\n(" << r.timeTaken << "ns per value)\"";
    fh << " with lines";
  }
  fh << "\n";
  fh << "unset multiplot\n";
  return 0;
}
