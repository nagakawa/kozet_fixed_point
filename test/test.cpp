/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <iostream>
#include <random>
#include <vector>

#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch.hpp>

#include "kozet_fixed_point/kfp.h"
#include "kozet_fixed_point/kfp_extra.h"
#include "kozet_fixed_point/kfp_string.h"
#include "kozet_fixed_point/random/kfp_wrapper.h"

TEST_CASE("Construct from integer", "[kfp][ctor]") {
  kfp::s16_16 k = 3;
  REQUIRE(k.underlying == 3 << 16);
  kfp::Fixed<int64_t, 32> y = 2457;
  REQUIRE(y.underlying == 2457LL << 32);
  kfp::frac32 f = 1;
  REQUIRE(f.underlying == 0);
}

TEST_CASE("Construct from another variable", "[kfp][ctor]") {
  kfp::s16_16 k = 5;
  kfp::s16_16 l = k;
  REQUIRE(l.underlying == 5 << 16);
  kfp::Fixed<int64_t, 25> m = l;
  REQUIRE(m.underlying == 5 << 25);
}

TEST_CASE("Construct with raw value", "[kfp][ctor]") {
  kfp::s16_16 m = kfp::s16_16::raw(5835);
  REQUIRE(m.underlying == 5835);
}

TEST_CASE("Basic arithmetic", "[kfp][arith]") {
  kfp::s16_16 k = 3;
  auto m = kfp::s16_16::raw(5835);
  REQUIRE((k + m).underlying == ((3 << 16) + 5835));
  REQUIRE((k - m).underlying == ((3 << 16) - 5835));
  REQUIRE((k * m).underlying == 3 * 5835);
  REQUIRE((k / m).underlying == ((3LL << 32) / 5835));
  REQUIRE((k + 1).underlying == 4 << 16);
  REQUIRE((k - 1).underlying == 2 << 16);
  REQUIRE((k * 2).underlying == 6 << 16);
  REQUIRE((k / 4).underlying == 3 << 14);
  REQUIRE((-k).underlying == -3 * (1 << 16));
  REQUIRE((-(-k)).underlying == k.underlying);
  REQUIRE((7 + k).underlying == 10 << 16);
  REQUIRE((7 - k).underlying == 4 << 16);
  REQUIRE((2 * k).underlying == 6 << 16);
  REQUIRE((6 / k).underlying == 2 << 16);
}

TEST_CASE("Long multiply", "[kfp][arith]") {
  uint16_t a = 5835;
  REQUIRE(kfp::longMultiply(a, a) == 5835 * 5835);
  kfp::s16_16 b = kfp::s16_16::raw(132395827);
  REQUIRE(
      kfp::longMultiply(b, b) ==
      kfp::Fixed<int64_t, 32>::raw(17528655007013929ULL));
}

TEST_CASE("Bitwise operations", "[kfp][arith]") {
  kfp::s16_16 a = 5, b = 9;
  REQUIRE((a & b) == 1);
  REQUIRE((a | b) == 13);
  REQUIRE((a ^ b) == 12);
  // Due to obvious reasons, this isn't just -6
  REQUIRE(~a == kfp::s16_16::raw(~a.underlying));
}

TEST_CASE("Comparisons", "[kfp][arith]") {
  kfp::s16_16 k = 3;
  auto m = kfp::s16_16::raw(5835);
  REQUIRE(k > 0);
  REQUIRE(m >= 0);
  REQUIRE(k > m);
  REQUIRE(m < k);
  REQUIRE(k >= m);
  REQUIRE(m <= k);
  REQUIRE(m == m);
  REQUIRE(m != m - 2);
}

TEST_CASE("Bit shifting", "[kfp][arith]") {
  kfp::s16_16 x = 3;
  REQUIRE(x << 1 == 6);
  REQUIRE(x << 3 == 24);
  REQUIRE(x >> 2 == kfp::s16_16::raw(3 << 14));
}

TEST_CASE("Rounding", "[kfp][arith]") {
  kfp::s16_16 p = kfp::s16_16::raw(12345678);
  REQUIRE(p.floor() == 12345678 / 65536);
  REQUIRE(p.ceil() == p.floor() + 1);
  REQUIRE(p.round() == p.floor());
  kfp::s16_16 q = kfp::s16_16::raw(32768);
  REQUIRE(q.floor() == 0);
  REQUIRE(q.ceil() == 1);
  REQUIRE(q.round() == 1);
  kfp::s16_16 r = kfp::s16_16::raw(1);
  REQUIRE(r.floor() == 0);
  REQUIRE(r.ceil() == 1);
  REQUIRE(r.round() == 0);
  kfp::frac32 f = 0;
  REQUIRE(f.floor() == 0);
  REQUIRE(f.ceil() == 0);
  REQUIRE(f.round() == 0);
  kfp::frac32 g = kfp::frac32::raw(0x92929292);
  REQUIRE(g.floor() == 0);
  REQUIRE(g.ceil() == 1);
  REQUIRE(g.round() == 1);
  kfp::Fixed<int32_t, 0> i = 56;
  REQUIRE(i.floor() == 56);
  REQUIRE(i.ceil() == 56);
  REQUIRE(i.round() == 56);
  auto b = kfp::Fixed<int64_t, 48>::raw(0x123'4567'89AB'CDEFULL);
  REQUIRE(b.floor() == 0x123);
  REQUIRE(b.ceil() == 0x124);
  REQUIRE(b.round() == 0x123);
  auto h = kfp::Fixed<int32_t, 32>::raw(-2'000'000'000);
  REQUIRE(h.floor() == -1);
  REQUIRE(h.ceil() == 0);
  REQUIRE(h.round() == -1);
}

TEST_CASE("Conversion to double", "[kfp][arith]") {
  kfp::s16_16 p = kfp::s16_16::raw(6554);
  REQUIRE(p.toDouble() == Approx(0.1).epsilon(0.0001));
}

TEST_CASE("Fixed literals", "[kfp][ctor]") {
  using namespace kfp::literals;
  kfp::s16_16 threeHalfs = "1.5"_s16_16;
  kfp::s16_16 oneTenth = "0.1"_s16_16;
  kfp::s16_16 oneFifth = ".2"_s16_16;
  kfp::s16_16 three = "3."_s16_16;
  kfp::s16_16 six = "6"_s16_16;
  REQUIRE(threeHalfs.underlying == 3 << 15);
  REQUIRE(oneTenth.toDouble() == Approx(0.1).epsilon(0.0001));
  REQUIRE(oneFifth.toDouble() == Approx(0.2).epsilon(0.0001));
  REQUIRE(three == 3);
  REQUIRE(six == 6);
}

TEST_CASE("Multiplication of fractional types", "[kfp][arith]") {
  using namespace kfp::literals;
  kfp::frac32 x = "0.77"_frac32;
  REQUIRE((x * x).toDouble() == Approx(0.77 * 0.77));
}

TEST_CASE("Fractions", "[kfp][arith][ctor]") {
  kfp::frac32 twoFifths = kfp::frac32::fraction(2, 5);
  REQUIRE(twoFifths.toDouble() == Approx(0.4).epsilon(ldexp(1, -32)));
  auto elevenSevenths = kfp::Fixed<uint32_t, 31>::fraction(11, 7);
  REQUIRE(elevenSevenths.toDouble() == Approx(11.0 / 7).epsilon(ldexp(1, -31)));
}

TEST_CASE("Trigonometry", "[extra][trig]") {
  using namespace kfp::literals;
  /* local */ {
    kfp::frac32 t = "0.125"_frac32; // 45 degrees
    auto [c, s] = kfp::sincos(t);
    REQUIRE(c.toDouble() == Approx(sqrt(2) / 2).epsilon(1.0 / 65536));
    REQUIRE(s.toDouble() == Approx(sqrt(2) / 2).epsilon(1.0 / 65536));
  }
  /* local */ {
    kfp::s16_16 c = 1, s = 2;
    auto [r, t] = kfp::rectp(c, s);
    REQUIRE(r.toDouble() == Approx(sqrt(5)).epsilon(1.0 / 65536));
    REQUIRE(t.toDouble() * 2 * M_PI == Approx(atan2(2, 1)));
  }
}

TEST_CASE("Trigonometry (performance)", "[extra][trig][!benchmark]") {
  kfp::frac32 sink = 0;
  BENCHMARK("0x100000 operations") {
    kfp::s2_30 c, s;
    kfp::s16_16 cf, sf, r;
    kfp::frac32 t, i = 0;
    do {
      kfp::sincos(i, c, s);
      cf = (kfp::s16_16) c;
      sf = (kfp::s16_16) s;
      kfp::rectp(cf, sf, r, t);
      sink += t;
      i += kfp::frac32::raw(0x1000);
    } while (i != kfp::frac32(0));
  };
  std::cerr << sink << "\n";
}

TEST_CASE("Square roots (integers)", "[extra][sqrt]") {
  uint64_t n = 123456789;
  uint64_t n2 = n * n;
  uint64_t na = kfp::sqrtiInt(n2);
  uint64_t nb = kfp::sqrtiFloat(n2);
  REQUIRE(na == n);
  REQUIRE(nb == n);
  REQUIRE(kfp::sqrtiInt(n2 - 1) == n - 1);
  REQUIRE(kfp::sqrtiFloat(n2 - 1) == n - 1);
  uint64_t x = 3542554395382948593ULL;
  REQUIRE(kfp::sqrtiInt(x) == kfp::sqrtiFloat(x));
}

TEST_CASE("Square roots (fixed)", "[extra][sqrt]") {
  kfp::s16_16 n = 7;
  kfp::s16_16 sqrtn = kfp::sqrt(n);
  REQUIRE(sqrtn.toDouble() == Approx(sqrt(7)).epsilon(1.0 / 256));
}

TEST_CASE("Hypotenuse", "[extra][sqrt]") {
  kfp::s16_16 x = 3, y = 7;
  kfp::s16_16 dist = kfp::hypot(x, y);
  REQUIRE(dist.toDouble() == Approx(hypot(3, 7)).epsilon(1.0 / 65536));
  auto dist2 = kfp::hypot2(x, y);
  REQUIRE(dist2 == (3 * 3 + 7 * 7));
}

TEST_CASE("Square root (perforamnce)", "[extra][sqrt][!benchmark]") {
  int sink = 0;
  BENCHMARK("Integer algorithm") {
    for (size_t i = 0; i < 1000000; ++i) { sink += kfp::sqrtiInt(rand()); }
  };
  BENCHMARK("Algorithm with floating-point operations") {
    for (size_t i = 0; i < 1000000; ++i) { sink += kfp::sqrtiFloat(rand()); }
  };
  std::cout << "sink = " << sink << "\n";
}

TEST_CASE("String parsing", "[string][parse]") {
  using namespace std::literals::string_view_literals;
  using namespace kfp::literals;
  auto four = kfp::fromString<kfp::s16_16>("4"sv);
  auto pi = kfp::fromString<kfp::s16_16>("3.14159265358979"sv);
  auto beforeZero = kfp::fromString<kfp::s16_16>(" -0.5"sv);
  auto nine = kfp::fromString<kfp::s16_16>("\t\t 9."sv);
  auto quarter = kfp::fromString<kfp::s16_16>(".25"sv);
  auto invalid1 = kfp::fromString<kfp::s16_16>(""sv);
  auto invalid2 = kfp::fromString<kfp::s16_16>("."sv);
  auto invalid3 = kfp::fromString<kfp::s16_16>("99999"sv);
  auto invalid4 = kfp::fromString<kfp::s16_16>("\n\n\n\n."sv);
  REQUIRE(four.value() == "4"_s16_16);
  REQUIRE(pi.value() == "3.14159265358979"_s16_16);
  REQUIRE(beforeZero.value() == -"0.5"_s16_16);
  REQUIRE(nine.value() == "9"_s16_16);
  REQUIRE(quarter.value() == "0.25"_s16_16);
  REQUIRE(!invalid1.has_value());
  REQUIRE(!invalid2.has_value());
  REQUIRE(!invalid3.has_value());
  REQUIRE(!invalid4.has_value());
}

TEST_CASE("String unparsing", "[string][unparse]") {
  using namespace kfp::literals;
  auto four = kfp::toString("4"_s16_16);
  auto pi = kfp::toString("3.14159265358979"_s16_16);
  auto beforeZero = kfp::toString(-"0.5"_s16_16);
  auto quarter = kfp::toString("0.25"_s16_16);
  auto pre1 = kfp::toString(kfp::s16_16::raw(65535));
  REQUIRE(four == "4");
  REQUIRE(pi == "3.14159");
  REQUIRE(beforeZero == "-0.5");
  REQUIRE(quarter == "0.25");
  REQUIRE(pre1 == "0.99998");
}
